cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

find_package(Boost REQUIRED COMPONENTS unit_test_framework)

add_subdirectory(src)
add_subdirectory(test)
