#ifndef LED_BASE_GATE_HH
#define LED_BASE_GATE_HH

#include <AbstractGate.hh>
#include <Typedefs.h>
#include <IObserver.hh>
#include <Pin.hh>
#include <arv.hh>

#include <boost/signals2.hpp>
#include <vector>

namespace Core {
    class BaseGate: public AbstractGate {
        using SignalToObserver = boost::signals2::signal<void(PinId, Impl::PinChange)>;
    public:
        BaseGate();
        BaseGate(std::size_t inputCount, std::size_t outputCount);

        void ConnectTo(AbstractGatePtr destinationGate, PinNumber outPin,
                       PinNumber inputPin) override;

        void DisconnectInput(PinNumber inputPin) override;

        void HandleIncomingConnection(AbstractGate *connectingGate, PinNumber inputPin, PinNumber outputPin) override;

        void HandleOutputDisconnected(PinNumber outputPin) override;

        GateId GetId() const override { return m_id; }

        void SetComponentObserver(Impl::IComponentObserver &gate) override;

        void RemoveComponentObserver() override;

        InputPinArrayView GetInputsView() const override { return m_inputs; }
        OutputPinArrayView GetOutputsView() const override { return m_outputs; }

        ~BaseGate() override {}

    protected:
        std::vector<InputPin> m_inputs;
        std::vector<OutputPin> m_outputs;
    private:
        GateId m_id;
        SignalToObserver m_signalInputChange;
        SignalToObserver m_signalOutputChange;
    };
}

#endif //	LED_BASE_GATE_HH
