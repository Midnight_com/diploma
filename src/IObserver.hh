#ifndef LED_IOBSERVER_HH
#define LED_IOBSERVER_HH
#include <Typedefs.h>
#include <PinId.hh>

namespace Core {

namespace Impl {

enum class PinChange {
    Connected = 0,
    Disconnected,
    MaxChange
};

class IComponentObserver {
public:
    virtual void HandleComponentInputChange(PinId pinId, PinChange change) = 0;
    virtual void HandleComponentOutputChange(PinId pinId, PinChange change) = 0;

    virtual ~IComponentObserver() {}
};

}

}
#endif
