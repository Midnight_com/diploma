#ifndef LED_ARRAY_SPAN_HH
#define LED_ARRAY_SPAN_HH

namespace Core {

namespace Utils {

template <typename ColType>
class ArraySpan {
public:
    explicit ArraySpan(const ColType &);
};

}

}
#endif
