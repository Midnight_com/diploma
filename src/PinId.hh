#ifndef LED_PIN_ID_HH
#define LED_PIN_ID_HH

#include <Typedefs.h>

namespace Core {

class AbstractGate;

class PinId {
public:
    PinId();

    bool operator==(const PinId &right) const {
        return right.m_gate == m_gate &&
                right.m_number == m_number;
    }

    void SetGate(AbstractGate *gate) { m_gate = gate; }
    AbstractGate *GetGate() { return m_gate; }
    const AbstractGate *GetGate() const { return m_gate; }

    void SetPinNumber(PinNumber num) { m_number = num; }
    PinNumber GetPinNumber() const { return m_number; }

    bool IsValid() const { return m_gate != nullptr; }

    bool SetInvalid() {
        m_gate = nullptr;
        m_number = 0;
    }
private:
    AbstractGate *m_gate;
    PinNumber m_number;
};

}
#endif
