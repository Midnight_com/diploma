#ifndef LED_ABSTRACT_GATE_HH
#define LED_ABSTRACT_GATE_HH

#include <Typedefs.h>
#include <boost/smart_ptr.hpp>
#include <boost/smart_ptr/intrusive_ref_counter.hpp>
#include <Pin.hh>

namespace Core {

namespace Impl {
    class IComponentObserver;
}

    class AbstractGate: public boost::intrusive_ref_counter<AbstractGate>
    {
	public:
	    virtual void ConnectTo(AbstractGatePtr destinationGate, PinNumber outPin, PinNumber inputPin) = 0;
            virtual void HandleIncomingConnection(AbstractGate *connectingGate, PinNumber inPin, PinNumber outPin) = 0;
        virtual void HandleOutputDisconnected(PinNumber inputPin) = 0;
	    virtual void DisconnectInput(PinNumber inputPin) = 0;
            virtual GateId GetId() const = 0;

        virtual void SetComponentObserver(Impl::IComponentObserver &gate) = 0;
        virtual void RemoveComponentObserver() = 0;

        virtual InputPinArrayView GetInputsView() const = 0;
        virtual OutputPinArrayView GetOutputsView() const = 0;

        virtual void Evaluate() const = 0;

        virtual ~AbstractGate() {}
    };
}

#endif //	LED_ABSTRACT_GATE_HH
