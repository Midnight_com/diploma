#ifndef LED_UTILS_HH
#define LED_UTILS_HH

#include <PinId.hh>

namespace Core {

class AbstractGate;

namespace Utils {

PinId MakePinId(AbstractGate *gate, PinNumber number);

}
}
#endif
