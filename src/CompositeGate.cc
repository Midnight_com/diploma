#include <CompositeGate.hh>
#include <IObserver.hh>
#include <utility>
#include <Utils.hh>

using namespace Core;

void CompositeGate::AddGate(AbstractGatePtr gate) {
    auto inserted = m_components.insert(std::make_pair(gate->GetId(), gate));

    if (inserted.second) {
        gate->SetComponentObserver( *this );
        AddComponentPins( *gate );
    }
}

void CompositeGate::RemoveGate(GateId gateId) {
    auto itr = m_components.find(gateId);

    if (itr != m_components.end()) {
        AbstractGatePtr &gate = itr->second;
        gate->RemoveComponentObserver();
        RemoveComponentPins( *gate );
        m_components.erase(itr);
    }
}

void CompositeGate::HandleComponentInputChange(PinId pinId, Impl::PinChange change) {
    BOOST_ASSERT(pinId.IsValid());

    if (Impl::PinChange::Connected == change) {
        auto input = std::find_if(m_inputs.begin(), m_inputs.end(),
                                  [&pinId](const InputPin &pin) -> bool { return pin.GetSelfId() == pinId; });
        BOOST_ASSERT(input != m_inputs.end());
        m_inputs.erase(input);
    }

    if (Impl::PinChange::Disconnected == change) {
        InputPin input;
        input.SetPinId(std::move(pinId));
        m_inputs.push_back(input);
    }
}

void CompositeGate::HandleComponentOutputChange(PinId pinId, Impl::PinChange change) {
    BOOST_ASSERT(pinId.IsValid());

    if (Impl::PinChange::Connected == change) {
        auto output = std::find_if(m_outputs.begin(), m_outputs.end(),
                                   [&pinId](const OutputPin &pin) -> bool { return pin.GetSelfId() == pinId; });
        BOOST_ASSERT(output != m_outputs.end());

        m_outputs.erase(output);
    }

    if (Impl::PinChange::Disconnected == change) {
        OutputPin out;
        out.SetPinId(std::move(pinId));
        m_outputs.push_back(out);
    }
}

void CompositeGate::AddComponentPins(const AbstractGate &gate) {
    auto inputs = gate.GetInputsView();
    for (auto &input : inputs) {
        if (input.IsEmpty()) {
            m_inputs.push_back(input);
        }
    }

    auto outputs = gate.GetOutputsView();
    for (auto &output : outputs) {
        if (output.IsEmpty()) {
            m_outputs.push_back(output);
        }
    }
}

void CompositeGate::RemoveComponentPins(const AbstractGate &gate) {
    auto inputs = gate.GetInputsView();

    for (auto &input : inputs) {
        auto findRes = std::find_if(m_inputs.begin(), m_inputs.end(),
                                    [&input](const InputPin &pin) -> bool { return pin.GetSelfId() == input.GetSelfId(); });

        if (m_inputs.end() == findRes)
            continue;

        m_inputs.erase(findRes);
    }

    auto outputs = gate.GetOutputsView();
    for (auto &output : outputs) {
        auto findRes = std::find_if(m_outputs.begin(), m_outputs.end(),
                                    [&output](const OutputPin &pin) -> bool { return pin.GetSelfId() == output.GetSelfId(); });

        if (m_outputs.end() == findRes)
            continue;

        m_outputs.erase(findRes);
    }
}
