#ifndef LED_TYPEDEFS_HH
#define LED_TYPEDEFS_HH

#include <cstdint>
#include <boost/uuid/uuid.hpp>
#include <boost/smart_ptr.hpp>
#include <array>

namespace Core {
    using PinNumber = std::size_t;
    using GateId = boost::uuids::uuid;

    class AbstractGate;
    using AbstractGatePtr = boost::intrusive_ptr<AbstractGate>;
}

#endif //	LED_TYPEDEFS_HH
