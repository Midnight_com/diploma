#include <BaseGate.hh>
#include <Utils.hh>
#include <boost/uuid/uuid_generators.hpp>

using namespace Core;

BaseGate::BaseGate(): m_id{boost::uuids::random_generator()()} {}

BaseGate::BaseGate(std::size_t inputCount, std::size_t outputCount): m_id{boost::uuids::random_generator()()} {
    m_inputs.resize(inputCount);
    for (std::size_t i = 0; i < inputCount; ++i) {
        m_inputs[i].SetPinId(Utils::MakePinId(this, i));
    }

    m_outputs.resize(outputCount);
    for (std::size_t i = 0; i < outputCount; ++i) {
        m_outputs[i].SetPinId(Utils::MakePinId(this, i));
    }
}

void BaseGate::ConnectTo(AbstractGatePtr destinationGate, PinNumber outPin,
               PinNumber inputPin) {
    auto &output = m_outputs[outPin];

    if (outPin >= m_outputs.size()) {
        // TODO throw something
    }

    if (!output.IsEmpty()) {
        // TODO throw something
    }

    destinationGate->HandleIncomingConnection(this, inputPin, outPin);
    output.SetConnectedPinId(Utils::MakePinId(destinationGate.get(), inputPin));
    m_signalOutputChange(output.GetSelfId(), Impl::PinChange::Connected);
}

void BaseGate::DisconnectInput(PinNumber inputPin) {
    if (inputPin >= m_inputs.size()) {
        // TODO throw something
    }

    auto &pin = m_inputs[inputPin];

    if (!pin.IsEmpty()) {
        auto connectedGate = pin.GetConnectedGate();
        auto pinNumber = pin.GetOutputPinNumber();

        BOOST_ASSERT(connectedGate != nullptr);

        connectedGate->HandleOutputDisconnected(pinNumber);

        pin.Clear();
    }

    m_signalInputChange(pin.GetSelfId(), Impl::PinChange::Disconnected);
}

void BaseGate::HandleIncomingConnection(AbstractGate *connectingGate, PinNumber inputPin, PinNumber outputPin) {
    // TODO check for vacantness
    auto &pin = m_inputs[inputPin];
    pin.SetConnectedPinId(Utils::MakePinId(connectingGate, outputPin));
    m_signalInputChange(pin.GetSelfId(), Impl::PinChange::Connected);
}

void BaseGate::HandleOutputDisconnected(PinNumber outputPin) {
    auto &pin = m_outputs[outputPin];
    pin.Clear();
    m_signalOutputChange(pin.GetSelfId(), Impl::PinChange::Disconnected);
}

void BaseGate::SetComponentObserver(Impl::IComponentObserver &gate) {
    m_signalInputChange.connect([&gate](PinId id, Impl::PinChange change) -> void {
        gate.HandleComponentInputChange(id, change);
    });

    m_signalOutputChange.connect([&gate](PinId id, Impl::PinChange change) -> void {
        gate.HandleComponentOutputChange(id, change);
    });
}

void BaseGate::RemoveComponentObserver() {
    m_signalInputChange.disconnect_all_slots();
    m_signalOutputChange.disconnect_all_slots();
}
