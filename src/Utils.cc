#include <Utils.hh>

namespace Core {

namespace Utils {

PinId MakePinId(AbstractGate *gate, PinNumber number) {
    PinId id;

    id.SetGate(gate);
    id.SetPinNumber(number);

    return id;
}

}
}
