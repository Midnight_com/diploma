#ifndef LED_STANDARD_GATES_HH
#define LED_STANDARD_GATES_HH

#include <BaseGate.hh>

namespace Core {

class AndGate final : public BaseGate {
public:
    AndGate(): BaseGate(2, 1) {}
    void Evaluate() const override {}
};

}

#endif
