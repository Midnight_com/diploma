#ifndef LED_COMPOSITE_GATE_HH
#define LED_COMPOSITE_GATE_HH

#include <BaseGate.hh>
#include <Typedefs.h>
#include <PinId.hh>
#include <IObserver.hh>
#include <unordered_map>
#include <boost/functional/hash.hpp>

namespace std {

template<>
struct hash<Core::GateId> {
    std::size_t operator()(Core::GateId id) const {
        return boost::hash<Core::GateId>()(id);
    }
};

}

namespace Core {

class CompositeGate final : public BaseGate,
                      public Impl::IComponentObserver
{
    using ComponentsMap = std::unordered_map<GateId, AbstractGatePtr>;
public:
    void HandleComponentInputChange(PinId pinId, Impl::PinChange change) override;
    void HandleComponentOutputChange(PinId pinId, Impl::PinChange change) override;

    void AddGate(AbstractGatePtr gate);
    void RemoveGate(GateId gateId);

    void Evaluate() const override {}

    virtual ~CompositeGate() override {}

private:
    ComponentsMap m_components;

    void AddComponentPins(const AbstractGate &gate);
    void RemoveComponentPins(const AbstractGate &gate);
};

}

#endif
