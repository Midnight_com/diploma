#ifndef LED_PIN_HH
#define LED_PIN_HH

#include <cstdint>
#include <array>
#include <Typedefs.h>
#include <PinId.hh>
#include <arv.hh>

namespace Core {
    template <bool IsInput>
    class Pin;
    
    class PinMembers {
    protected:
        PinId m_selfId;
        PinId m_connectedId;
    public:
        void Clear() {
            m_connectedId.SetInvalid();
            m_selfId.SetInvalid();
        }

        bool IsEmpty() const { return !m_connectedId.IsValid() || !m_selfId.IsValid(); }

        void SetPinId(PinId &&id) { m_selfId = id; }
        void SetConnectedPinId(PinId &&id) { m_connectedId = id; }

        void SetConnectedGate(AbstractGate *gate) { m_connectedId.SetGate(gate); }
        AbstractGate *GetConnectedGate() { return m_connectedId.GetGate(); }
        const AbstractGate *GetConnectedGate() const { return m_connectedId.GetGate(); }

        const PinId &GetSelfId() const { return m_selfId; }
    };

    template<>
    class Pin<true> : public PinMembers {
    public:
        void SetOutputNumber(PinNumber pinNumber) { m_connectedId.SetPinNumber(pinNumber); }
        PinNumber GetOutputPinNumber() const { return m_connectedId.GetPinNumber(); }
    };
    
    template<>
    class Pin<false> : public PinMembers {
    public:
        void SetInputNumber(PinNumber pinNumber) { m_connectedId.SetPinNumber(pinNumber); }
        PinNumber GetInputPinNumber() const { return m_connectedId.GetPinNumber(); }
    };
    
    using InputPin = Pin<true>;
    using OutputPin = Pin<false>;

    template <std::size_t Count>
    using InputPinArray = std::array<InputPin, Count>;

    template <std::size_t Count>
    using OutputPinArray = std::array<OutputPin, Count>;

    using InputPinArrayView = arv::array_view<InputPin>;
    using OutputPinArrayView = arv::array_view<OutputPin>;
}

#endif //	LED_PIN_HH
