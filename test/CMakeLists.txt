cmake_minimum_required(VERSION 3.3 FATAL_ERROR)
project(led_tests)

set(SOURCES main.cpp plain-gates-test.cpp)

add_executable(${PROJECT_NAME} "${SOURCES}")
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_STANDARD 11 _GLIBCXX_USE_CXX11_ABI 1)
target_compile_definitions(${PROJECT_NAME} PRIVATE BOOST_TEST_DYN_LINK LED_TESTS)

target_include_directories(${PROJECT_NAME} PRIVATE "${Boost_INCLUDE_DIRS}")
target_link_libraries(${PROJECT_NAME} PRIVATE engine "${Boost_LIBRARIES}")
