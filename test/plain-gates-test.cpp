#include <boost/test/unit_test.hpp>
#include <BaseGate.hh>
#include <array>
#include <stdexcept>
#include <StandardGates.hh>
#include <CompositeGate.hh>

using namespace Core;

BOOST_AUTO_TEST_SUITE(PlainGatesTests)

BOOST_AUTO_TEST_CASE(plain_gate_test) {
    AbstractGatePtr pg1(new AndGate());
    AbstractGatePtr pg2(new AndGate());
    bool eq = (pg1->GetId() == pg2->GetId());



    CompositeGate composite;
    composite.AddGate(pg1);
    composite.AddGate(pg2);

    pg1->ConnectTo(pg2, 0, 0);
    pg2->DisconnectInput(0);
}

BOOST_AUTO_TEST_SUITE_END()
